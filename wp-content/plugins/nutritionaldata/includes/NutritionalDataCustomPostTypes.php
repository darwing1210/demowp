<?php

/**
 * Plugin specific post types and taxonomies
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    nutritionaldata
 * @subpackage nutritionaldata/includes
 */

/**
 * Plugin specific post types and taxonomies
 *
 * Defines the plugin custom post type and relations
 *
 * @package    nutritionaldata
 * @subpackage nutritionaldata/includes
 * @author     Darwing Medina <darwing1210@gmail.com>
 */
class NutritionalDataCustomPostTypes {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Nutritional Data Brand Post Type
	 *
	 * @since     1.0.0
	 */
	public function register_cpt_brand() {

		$labels = array(
			"name"					=> __( 'Brands', $this->plugin_name ),
			"singular_name"			=> __( 'Brand', $this->plugin_name ),
		);

		$args = array(
			"label" 				=> __( 'Brands', $this->plugin_name ),
			"labels" 				=> $labels,
			"description" 			=> "",
			"public" 				=> true,
			"publicly_queryable" 	=> true,
			"show_ui" 				=> true,
			"show_in_rest" 			=> true,
			"rest_base" 			=> "brands",
			"has_archive" 			=> false,
			"show_in_menu" 			=> true,
			"exclude_from_search" 	=> false,
			"capability_type" 		=> "post",
			"map_meta_cap" 			=> true,
			"hierarchical" 			=> true,
			"rewrite" 				=> array( "slug" => "brand", "with_front" => true ),
			"query_var" 			=> true,
			"menu_icon" 			=> "dashicons-megaphone",
			"supports" 				=> array( "title", "editor", "thumbnail" ),
		);
		register_post_type( "brand", $args );
	}

	/**
	 * Nutritional Data Product Post Type
	 *
	 * @since     1.0.0
	 */
	public function register_cpt_product() {
		
		$labels = array(
			"name" 					=> __( 'Products', $this->plugin_name ),
			"singular_name" 		=> __( 'Product', $this->plugin_name ),
		);

		$args = array(
			"label" 				=> __( 'Products', $this->plugin_name ),
			"labels" 				=> $labels,
			"description" 			=> "",
			"public" 				=> true,
			"publicly_queryable" 	=> true,
			"show_ui" 				=> true,
			"show_in_rest" 			=> true,
			"rest_base" 			=> "products",
			"has_archive" 			=> true,
			"show_in_menu" 			=> true,
			"exclude_from_search" 	=> false,
			"capability_type" 		=> "post",
			"map_meta_cap" 			=> true,
			"hierarchical" 			=> false,
			"rewrite" 				=> array( "slug" => "product", "with_front" => true ),
			"query_var"				=> true,
			"menu_icon" 			=> "dashicons-cart",		
			"supports" 				=> array( "title", "editor", "thumbnail" ),
		);
		register_post_type( "product", $args );
	}

	/**
	 * Nutritional Data Product Category Taxonomy
	 *
	 * @since     1.0.0
	 */
	public function register_taxonomy_product_category() {
	
		$labels = array(
			"name" 					=> __( 'Product Categories', $this->plugin_name ),
			"singular_name" 		=> __( 'Product Category', $this->plugin_name ),
		);

		$args = array(
			"label" 				=> __( 'Product Categories', $this->plugin_name ),
			"labels" 				=> $labels,
			"public" 				=> true,
			"hierarchical" 			=> false,
			"label" 				=> "Product Categories",
			"show_ui" 				=> true,
			"query_var" 			=> true,
			"rewrite" 				=> array( 'slug' => 'product-category', 'with_front' => true ),
			"show_admin_column" 	=> false,
			"show_in_rest" 			=> true,
			"rest_base" 			=> "product-category",
			"show_in_quick_edit" 	=> true,
		);
		register_taxonomy( "product-category", array( "product" ), $args );

	}

	/**
	 * Product Brand Metabox
	 * Adds a Metabox to select product brand
	 *
	 * @since     1.0.0
	 */
	public function product_meta_boxes() {
		
		add_meta_box( 
			'product-parent', 
			'Brand', 
			array($this,'product_attributes_meta_box'),
			'product', 
			'side', 
			'high' 
		);	
	}

	public function product_attributes_meta_box( $post ) {

		$post_type_object = get_post_type_object( $post->post_type );
		$pages = wp_dropdown_pages( 
			array( 
				'post_type' 		=> 'brand', 
				'selected' 			=> $post->post_parent, 
				'name' 				=> 'parent_id', 
				'show_option_none' 	=> __( '(no brand)' ), 
				'sort_column'		=> 'menu_order, post_title', 
				'echo' 				=> 0 
			) 
		);
		if ( ! empty( $pages ) ) {
			echo $pages;
		}
	}
	
	/**
	 * Product URL Rewrites rules
	 * Sets Product URL rules
	 *
	 * @since     1.0.0
	 */
	public function product_rewrite_rules() {
		add_rewrite_tag('%product%', '([^/]+)', 'product=');
		add_permastruct('product', '/products/%brand%/%product%', false);
		add_rewrite_rule('^products/([^/]+)/([^/]+)/?','index.php?product=$matches[2]','top');
	}

	/**
	 * Updating Product Permalink
	 * Sets Product Permalinks, Replaces %brand% with product parent brand slug
	 *
	 * @since     1.0.0
	 * @param     string    $permalink       The name of this plugin.
	 * @param     object    $post    Current Post.
	 * @return    string    $permalink Modified Permalink.
	 */
	public function product_permalinks($permalink, $post) {
		
		$post_id = $post->ID;
		if ( $post->post_type != 'product' || empty($permalink) || in_array( $post->post_status, array('draft', 'pending', 'auto-draft') ) )
		 	return $permalink; // In case is not a Product post

		$parent = $post->post_parent;
		$parent_post = get_post( $parent );
		$permalink = str_replace('%brand%', $parent_post->post_name, $permalink);
		
		return $permalink;
	}

	/**
	 * Sets Single product template
	 *
	 * @since     1.0.0
	 * @param     object    $single    Current Post.
	 */
	public function product_custom_template($single) {

		global $wp_query, $post;
	    /* Checks for single template by post type */
	    if ( $post->post_type == 'product' ) {
	    	$template = 'public/templates/nutritionaldata-single-product.php';
	        if ( file_exists( plugin_dir_path( dirname( __FILE__ ) ) . $template ) )
	            return plugin_dir_path( dirname( __FILE__ ) ) . $template ;
	    }
	    return $single;
	}

}
