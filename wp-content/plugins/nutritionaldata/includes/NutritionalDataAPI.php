<?php

/**
 * Plugin Advance Custom Fields setup
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    nutritionaldata
 * @subpackage nutritionaldata/includes
 */

/**
 * Plugin API hooks
 *
 * Defines Plugin API endpoints
 *
 * @package    nutritionaldata
 * @subpackage nutritionaldata/includes
 * @author     Darwing Medina <darwing1210@gmail.com>
 */
class NutritionalDataAPI {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->product_endpoint();

	}

	/**
	 * Products Endpoint '/wp-json/wp/v2/products'
	 */
	public function product_endpoint() {
		register_rest_field( 'product',
        'brand',
	        array(
	            'get_callback'    => array( $this,'get_product_brand' ),
	            'update_callback' => null,
	            'schema'          => null,
	        )
	    );
	}

	/**
	 * Brands Endpoint '/wp-json/wp/v2/brands'
	 */
	public function brand_endpoint() {
		register_rest_field( 'brand',
        'products',
	        array(
	            'get_callback'    => array( $this,'get_brand_products' ),
	            'update_callback' => null,
	            'schema'          => null,
	        )
	    );
	}

	public function register_custom_endpoints() {

		$namespace = 'wp/v2';

		register_rest_route( $namespace, '/featured-products/', array(
	        'methods' => 'GET',
	        'callback' => array( $this, 'get_featured_products' ),
	    ) );

	    register_rest_route( $namespace, '/top-rated-products/', array(
	        'methods' => 'GET',
	        'callback' => array( $this, 'get_top_rated_products' ),
	    ) );

	    register_rest_field( 'product-category',
        'products',
	        array(
	            'get_callback'    => array( $this,'get_category_products' ),
	            'update_callback' => null,
	            'schema'          => null,
	        )
		 );
	
	}

	/**
	 * This function get product brand;
	 */
	public function get_brand_products( $object, $request) {
		
		$brand = get_post( $object[ 'id' ] );
		
		$products = get_posts( array(
				'post_type' 	=> 'product',
				'post_parent' 	=> $object[ 'id' ],
		) );

		return $products;
	}

	/**
	 * This function get product brand;
	 */
	public function get_product_brand( $object, $field_name, $request) {
		
		$product = get_post( $object[ 'id' ] );
		$brand_id = $product->post_parent;
		$brand_post = get_post($brand_id);

		$brand = array(
			'id' 	=> $brand_post->ID,
			'slug' 	=> $brand_post->post_name,
			'name' 	=> $brand_post->post_title,
			'link'	=> rest_url() . 'wp/v2/brands/' . $brand_post->ID
		);

		return $brand;
	}

	/**
	 * /wp-json/wp/v2/featured-products/
	*/

	public function get_featured_products() {
			
		$args = array(
			'post_type'		=> 'product',
			'meta_key'		=> 'featured',
			'meta_value'	=> true
		);

		$products = get_posts( $args );

		if ( empty( $products ) ) {
			return null;
		}

		return $products;
	}

	/**
	 * /wp-json/wp/v2/top-rated-products/
	*/

	public function get_top_rated_products( $request ) {
			
		$args = array(
			'posts_per_page'	=> 5,
			'post_type'			=> 'product',
			'orderby'			=> 'meta_value_num',
			'meta_key'			=> 'rate_count'
		);

		$products = get_posts( $args );
		$data = array();
		
		if ( $products ) {
		    foreach ( $products as $product ) {
				$data[] = array(
					'id' 	=> $product->ID,
					'slug' 	=> $product->post_name,
					'name' 	=> $product->post_title,
					'rate'	=> get_post_meta($product->ID, 'rate_count', true ),
					'link'	=> rest_url() . 'wp/v2/products/' . $product->ID
				);
			}
		}
		else if( empty( $products ) ) {
			return null;
		}

		return $data;
	}

	/**
	 * /wp-json/wp/v2/product-category/{{ id }}/
	*/

	public function get_category_products( $object, $field_name, $request ) {
			
		$args = array(
			'post_type'		=>	'product',
			'tax_query' 	=>  array(
				array(
	                'taxonomy' => 'product-category',
	                'field' => 'id',
	                'terms' => $object[ 'id' ],
	            ),
	        ),
		);

		$products = get_posts( $args );

		if ( empty( $products ) ) {
			return null;
		}
		
		return $products;
	}
}