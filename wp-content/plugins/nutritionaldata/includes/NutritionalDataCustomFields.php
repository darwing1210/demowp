<?php

/**
 * Plugin Advance Custom Fields setup
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    nutritionaldata
 * @subpackage nutritionaldata/includes
 */

/**
 * Plugin specific post types and taxonomies
 *
 * Defines Plugin Advance Custom fields
 *
 * @package    nutritionaldata
 * @subpackage nutritionaldata/includes
 * @author     Darwing Medina <darwing1210@gmail.com>
 */
class NutritionalDataCustomFields {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->product_custom_fields();

	}

	/**
	 * Register the product post type advance fields
	 */
	public function product_custom_fields() {

		if( function_exists("register_field_group") ) {
			
			register_field_group( array (
				'id' => 'acf_product-nutritional-data',
				'title' => 'Product Nutritional Data',
				'fields' => array (
					array (
						'key' => 'field_58575c5e5c12a',
						'label' => 'Serving Size',
						'name' => 'serving_size',
						'type' => 'number',
						'default_value' => 0,
						'placeholder' => 125,
						'prepend' => '',
						'append' => 'g',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58575e545c12c',
						'label' => 'Calories',
						'name' => 'calories',
						'type' => 'number',
						'instructions' => 'Amount Per serving',
						'default_value' => 0,
						'placeholder' => 65,
						'prepend' => '',
						'append' => '',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58575e8a5c12d',
						'label' => 'Calories from Fat',
						'name' => 'calories_from_fat',
						'type' => 'number',
						'instructions' => 'Amount Per serving',
						'default_value' => 0,
						'placeholder' => 2,
						'prepend' => '',
						'append' => '',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5857661f12ad7',
						'label' => 'Satured Fat',
						'name' => 'satured_fat',
						'type' => 'number',
						'instructions' => 'Amount Per serving',
						'default_value' => 0,
						'placeholder' => 0,
						'prepend' => '',
						'append' => 'g',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5857663e12ad8',
						'label' => 'Trans Fat',
						'name' => 'trans_fat',
						'type' => 'number',
						'instructions' => 'Amount per serving',
						'default_value' => 0,
						'placeholder' => 0,
						'prepend' => '',
						'append' => 'g',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5857666e12ad9',
						'label' => 'Cholesterol',
						'name' => 'cholesterol',
						'type' => 'number',
						'instructions' => 'Amount per serving',
						'default_value' => 0,
						'placeholder' => 0,
						'prepend' => '',
						'append' => 'mg',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5857668e12ada',
						'label' => 'Sodium',
						'name' => 'sodium',
						'type' => 'number',
						'instructions' => 'Amount per serving',
						'default_value' => 0,
						'placeholder' => 0,
						'prepend' => '',
						'append' => 'mg',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_585766c212adb',
						'label' => 'Dietary Fiber',
						'name' => 'dietary_fiber',
						'type' => 'number',
						'instructions' => 'Amount per serving',
						'default_value' => 0,
						'placeholder' => 0,
						'prepend' => '',
						'append' => 'g',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5857673212add',
						'label' => 'Sugars',
						'name' => 'sugars',
						'type' => 'number',
						'instructions' => 'Amount per serving',
						'default_value' => 0,
						'placeholder' => 0,
						'prepend' => '',
						'append' => 'g',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5857675712ade',
						'label' => 'Protein',
						'name' => 'protein',
						'type' => 'number',
						'instructions' => 'Amount per serving',
						'default_value' => 0,
						'placeholder' => 0,
						'prepend' => '',
						'append' => 'g',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5857676d12adf',
						'label' => 'Vitamin A',
						'name' => 'vitamin_a',
						'type' => 'number',
						'instructions' => 'Amount per serving',
						'default_value' => 0,
						'placeholder' => 0,
						'prepend' => '',
						'append' => '%',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5857679612ae0',
						'label' => 'Vitamin C',
						'name' => 'vitamin_c',
						'type' => 'number',
						'instructions' => 'Amount per serving',
						'default_value' => 0,
						'placeholder' => 0,
						'prepend' => '',
						'append' => '%',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_585767c012ae1',
						'label' => 'Calcium',
						'name' => 'calcium',
						'type' => 'number',
						'instructions' => 'Amount per serving',
						'default_value' => 0,
						'placeholder' => 0,
						'prepend' => '',
						'append' => '%',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_585767d112ae2',
						'label' => 'Iron',
						'name' => 'iron',
						'type' => 'number',
						'instructions' => 'Amount per serving',
						'default_value' => 0,
						'placeholder' => 0,
						'prepend' => '',
						'append' => '%',
						'min' => '',
						'max' => '',
						'step' => '',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'product',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'no_box',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));

			register_field_group(array (
				'id' => 'acf_product-display',
				'title' => 'Product display',
				'fields' => array (
					array (
						'key' => 'field_5858b6c5fdd23',
						'label' => 'Featured',
						'name' => 'featured',
						'type' => 'true_false',
						'instructions' => 'Is this product Featured?',
						'message' => '',
						'default_value' => 0,
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'product',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'side',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 1,
			));
		}
	}
}