<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    nutritionaldata
 * @subpackage nutritionaldata/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    nutritionaldata
 * @subpackage nutritionaldata/includes
 * @author     Your Name <email@example.com>
 */
class NutritionalData {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      NutritionalDataLoader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'nutritionaldata';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->define_cpt_hooks();
		$this->define_acf_hooks();
		$this->define_api_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Plugin_Name_Loader. Orchestrates the hooks of the plugin.
	 * - Plugin_Name_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/NutritionalDataLoader.php';

		/**
		 * The class responsible for defining all custom post types and taxonomies
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/NutritionalDataCustomPostTypes.php';

		/**
		 * The class responsible for defining all advance Custom fields
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/NutritionalDataCustomFields.php';

		/**
		 * The class responsible for Diet Calculations
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/NutritionalDataDiet.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/NutritionalDataAdmin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/NutritionalDataPublic.php';

		/**
		 * The class responsible for API definitions
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/NutritionalDataAPI.php';

		/**
		 * Include the TGM_Plugin_Activation class.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . '/admin/tgm-plugin-activation/class-tgm-plugin-activation.php';

		$this->loader = new NutritionalDataLoader();

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new NutritionalDataAdmin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'tgmpa_register', $plugin_admin,'nutritionaldata_register_required_plugins' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new NutritionalDataPublic( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		// Custom hooks
		$this->loader->add_action( 'review_post', $plugin_public, 'review_post' );
		$this->loader->add_action( 'get_nutritional_data', $plugin_public, 'get_nutritional_data' );

		// Shortcodes
		$this->loader->add_action( 'init', $plugin_public, 'register_shortcodes' );

		// Ajax hooks
		$this->loader->add_action( 'wp_ajax_product_rate', $plugin_public, 'product_rate' );
		$this->loader->add_action( 'wp_ajax_nopriv_product_rate', $plugin_public, 'must_login'  );
		$this->loader->add_action( 'wp_ajax_infinite_products', $plugin_public, 'infinite_products' );
		$this->loader->add_action( 'wp_ajax_nopriv_infinite_products', $plugin_public, 'infinite_products' );
	}

	/**
	 * Register all of the hooks related to custom post types
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_cpt_hooks() {

		$plugin_cpt = new NutritionalDataCustomPostTypes( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'init', $plugin_cpt, 'register_cpt_brand' );
		$this->loader->add_action( 'init', $plugin_cpt, 'register_cpt_product' );
		$this->loader->add_action( 'init', $plugin_cpt, 'register_taxonomy_product_category' );
		$this->loader->add_action( 'add_meta_boxes', $plugin_cpt, 'product_meta_boxes' );
		$this->loader->add_action( 'init', $plugin_cpt, 'product_rewrite_rules' );
		$this->loader->add_filter( 'post_type_link', $plugin_cpt, 'product_permalinks', 10, 3 );
		$this->loader->add_filter( 'single_template', $plugin_cpt, 'product_custom_template' );
	}

	/**
	 * Register all of the hooks related to Advance Custom Fields
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_acf_hooks() {

		$plugin_acf = new NutritionalDataCustomFields( $this->get_plugin_name(), $this->get_version() );
	}

	/**
	 * Register all of the hooks related to REST API plugin
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_api_hooks() {

		$plugin_api = new NutritionalDataAPI( $this->get_plugin_name(), $this->get_version() );
		$this->loader->add_action( 'rest_api_init', $plugin_api, 'product_endpoint' );
		$this->loader->add_action( 'rest_api_init', $plugin_api, 'brand_endpoint' );
		$this->loader->add_action( 'rest_api_init', $plugin_api, 'register_custom_endpoints' );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Plugin_Name_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
