<?php

/**
 * Nutritional Data Calculations
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    nutritionaldata
 * @subpackage nutritionaldata/includes
 */

/**
 * Defines Product Function Calculations
 *
 * @package    nutritionaldata
 * @subpackage nutritionaldata/includes
 * @author     Darwing Medina <darwing1210@gmail.com>
 */
class NutritionalDataDiet {

	/**
 	 * Defining recomended diet values based in 2000 calories
 	 * Taken from www.dsld.nlm.nih.gov/dsld/dailyvalue.jsp
 	*/

	const total_fat = 65; // g
	const satured_fat = 20; // g
	const trans_fat = 44; // g
	const cholesterol = 300; // mg
	const sodium = 2400; //mg
	const total_carbohydrate = 300; // g
	const dietary_fiber = 25; // g
	const sugars = 32; // g

	/**
 	 * This function calculates the porcentage of the product daily diet value
 	 * @return  float 	$porcentage      
 	*/
	static public function calculate_porcentage( $recomended_value, $product_value) {
		$porcentage = floor( ( $product_value * 100 ) / $recomended_value );
		return $porcentage;
	}
}
