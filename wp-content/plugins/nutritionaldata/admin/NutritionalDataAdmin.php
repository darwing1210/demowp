<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    nutritionaldata
 * @subpackage nutritionaldata/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    nutritionaldata
 * @subpackage nutritionaldata/admin
 * @author     Your Name <email@example.com>
 */
class NutritionalDataAdmin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the required plugins for this plugin.
	 */
	public function nutritionaldata_register_required_plugins() {

		/*
		 * Array of plugin arrays. Required keys are name and slug.
		 * If the source is NOT from the .org repo, then source is also required.
		 */
		$plugins = array(

			array(
				'name'               => 'Advanced Custom Fields',
				'slug'               => 'advanced-custom-fields', 
				'required'           => true,
				'version'            => '4.4.11',
				'force_activation'   => true,
				'force_deactivation' => false,
				'is_callable'        => 'acf',
			),
			array(
				'name'               => 'WordPress REST API (Version 2)', 
				'slug'               => 'rest-api', 
				'required'           => true,
				'version'            => '2.0-beta15',
				'force_activation'   => true,
				'force_deactivation' => false,
			),
			array(
				'name'               => 'ACF to REST API', 
				'slug'               => 'acf-to-rest-api', 
				'required'           => true,
				'version'            => '2.2.1',
				'force_activation'   => true,
				'force_deactivation' => false,
			),
		);

		$config = array(
			'id'           => 'nutritionaldata',                 // Unique ID for hashing notices for multiple instances of TGMPA.
			'default_path' => "plugin_dir_path( __FILE__ ) . 'plugins/'",                      // Default absolute path to bundled plugins.
			'menu'         => 'tgmpa-install-plugins', // Menu slug.
			'parent_slug'  => 'plugins.php',            // Parent menu slug.
			'capability'   => 'manage_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
			'has_notices'  => true,                    // Show admin notices or not.
			'dismissable'  => false,
			'is_automatic' => true,                   // Automatically activate plugins after installation or not.
		);
		tgmpa( $plugins, $config );
	}
}
