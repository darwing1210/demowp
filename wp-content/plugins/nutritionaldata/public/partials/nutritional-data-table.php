<div class="ntd-table-wrapper">
	<table class="ntd-table ntd-mb0">
		<thead>
			<tr>
				<th colspan="2"><h1 class="ntd-ma0 ntd-text-left">Nutrition Facts</h1></th>
			</tr>
			<tr>
				<th colspan="2" class="ntd-text-left">Serving Size <?php the_field('serving_size')?> </th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="2"><small><strong>Amount Per Saving</strong></small></td>
			</tr>
			<tr>
				<td><strong>Calories </strong><?php the_field('calories')?></td>
				<td>Calories from Fat <?php the_field('calories_from_fat') ?></td>
			</tr>
			<tr>
				<td colspan="2" class="ntd-text-right"><strong>% Daily Value*</strong></td>
			</tr>
			<tr>
				<td><strong>Total Fat </strong>
					<?php 
						$total_fat = get_field('satured_fat') + get_field('trans_fat');
						echo $total_fat . 'g';
					?>
				</td>
				<td class="ntd-text-right">
					<?php 
						echo $diet::calculate_porcentage(
								$diet::total_fat, 
								$total_fat
							) . '%'; 
					?>
				</td>
			</tr>
			<tr>
				<td class="ntd-pl15">Saturated Fat <?php the_field('satured_fat') ?>g</td>
				<td class="ntd-text-right">
					<?php 
						echo $diet::calculate_porcentage(
								$diet::satured_fat, 
								get_field('satured_fat')
							) . '%'; 
					?>
				</td>
			</tr>
			<tr>
				<td class="ntd-pl15">Trans Fat <?php the_field('trans_fat') ?>g</td>
				<td class="ntd-text-right">
					<?php 
						echo $diet::calculate_porcentage(
								$diet::trans_fat, 
								get_field('trans_fat')
							) . '%';
					?>
				</td>
			</tr>
			<tr>
				<td><strong>Cholesterol </strong><?php the_field('cholesterol') ?>g</td>
				<td class="ntd-text-right">
					<?php 
						echo $diet::calculate_porcentage(
								$diet::cholesterol, 
								get_field('cholesterol')
							) . '%';
					?>
				</td>
			</tr>
			<tr>
				<td><strong>Sodium </strong><?php the_field('sodium') ?>mg</td>
				<td class="ntd-text-right">
					<?php 
						echo $diet::calculate_porcentage(
								$diet::sodium, 
								get_field('sodium')
							) . '%';
					?>
				</td>
			</tr>
			<tr>
				<td><strong>Total Carbohydrate </strong>
					<?php 
						$total_carbohydrate = get_field('dietary_fiber') + get_field('sugars');
						echo $total_carbohydrate . 'g';
					?>
				</td>
				<td class="ntd-text-right">
					<?php 
						echo $diet::calculate_porcentage(
								$diet::total_carbohydrate, 
								$total_carbohydrate
							) . '%'; 
					?>
				</td>
			</tr>
			<tr>
				<td class="ntd-pl15">Dietary Fiber <?php the_field('dietary_fiber') ?>g</td>
				<td class="ntd-text-right">
					<?php 
						echo $diet::calculate_porcentage(
								$diet::dietary_fiber, 
								get_field('dietary_fiber')
							) . '%';
					?>
				</td>
			</tr>
			<tr>
				<td class="ntd-pl15">Sugars <?php the_field('sugars') ?>g</td>
				<td class="ntd-text-right">
					<?php 
						echo $diet::calculate_porcentage(
								$diet::sugars, 
								get_field('sugars')
							) . '%';
					?>
				</td>
			</tr>
			<tr>
				<td colspan="2"><strong>Protein </strong><?php the_field('protein') ?>g</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td>Vitamin A <span class="ntd-text-right"><?php the_field('vitamin_a') ?>%</span></td>
				<td>Vitamin C <span class="ntd-text-right"><?php the_field('vitamin_c') ?>%</span></td>
			</tr>
			<tr>
				<td>Calcium <span class="ntd-text-right"><?php the_field('calcium') ?>%</span></td>
				<td>Iron <span class="ntd-text-right"><?php the_field('iron') ?>%</span></td>
			</tr>
			<tr>
				<td colspan="2"><small>* Percent Daily Values are based on a 2,000 calorie diet. Your daily values may be higher or lower depending on your colorie needs</small></td>
			</tr>
			<tr>
				<td colspan="2"><a class="ntd-foot-button ntd-text-center" href="http://NutritionData.com" target="_blank"><strong>NutritionData.com</strong></a></td>
			</tr>
		</tfoot>
	</table>
</div>