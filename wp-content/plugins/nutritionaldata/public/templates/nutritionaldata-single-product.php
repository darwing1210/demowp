<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package nutritionaldata
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post(); ?>
			
			<div class="entry-content">
			<div class="ntd-container">
				<h1><?php the_title(); ?></h1>
				<?php do_action('review_post');
				the_post_thumbnail( 'large' );
				the_content();
				do_action('get_nutritional_data');
		endwhile; ?>
		</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();