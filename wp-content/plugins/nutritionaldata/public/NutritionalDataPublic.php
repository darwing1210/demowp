<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    nutritionaldata
 * @subpackage nutritionaldata/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    nutritionaldata
 * @subpackage nutritionaldata/public
 * @author     Your Name <email@example.com>
 */
class NutritionalDataPublic {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in NutritionalDataLoader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/nutritionaldata.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in NutritionalDataLoader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The NutritionalDataLoader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( 'load_products', plugin_dir_url( __FILE__ ) . '/js/inifinite_products_loader.js', array( 'jquery' ), $this->version, false );

		wp_localize_script('load_products', 'ajax_var', array(
		    'url' => admin_url('admin-ajax.php'),
		    'total_products' => (string) wp_count_posts('product')->publish,
		    'nonce' => wp_create_nonce('ajax-nonce')
		));

		wp_enqueue_script('rate', plugin_dir_url( __FILE__ ) .'/js/rate_product.js', array('jquery'), $this->version, true );
		
		wp_localize_script('rate', 'ajax_var', array(
		    'url' => admin_url('admin-ajax.php'),
		    'nonce' => wp_create_nonce('ajax-nonce')
		));

	}

	/**
	 * This function Register my custom shortcodes
	 *
	 * @since    1.0.0
	 */
	public function register_shortcodes() {
		add_shortcode( 'products', array( $this, 'infinite_products_shortcode') );
	}

	/**
	 * This function test if user already rated a post.
	 *
	 * @since    1.0.0
	 */
	public function hasAlreadyRated( $post_id, $meta_detail, $current_user) {
	   
	    // If user has already voted
	    foreach ( $meta_detail as $key ) {
	    	if( array_key_exists($current_user, $key) ) {
	        	return true;
    		}
	    }
	    return false;
	}

	/**
	 * Function ejecuted when user rate a post.
	 * Called with ajax.
	 *
	 * @since    1.0.0
	 */
	public function product_rate() {
	    // Check for nonce security

	    if ( is_user_logged_in() ) {
	    
		    $nonce = $_POST['nonce'];

		    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
		        die ( 'Cant access here!');
		     
		    if ( isset( $_POST['action'] ) ) {

		        $post_id = $_POST['product_id'];
		        $product_score = $_POST['product_score'];

		        $current_user = wp_get_current_user()->user_login;

		        $meta_count = get_post_meta( $post_id, "rate_count", true );
		        $meta_detail = get_post_meta( $post_id, "username_rate", true );

		        // delete_post_meta( $post_id, "username_rate");

		        if ( ! $this->hasAlreadyRated( $post_id, $meta_detail, $current_user) ) {

		        	$new_result = $meta_count + $product_score;

		        	$args = array( $current_user => $product_score );
	        		
	        		if ( is_array($meta_detail) ) 
					    $meta_detail[] = $args;
					else
					    $meta_detail = array($args);

	        		update_post_meta( 
	            		$post_id, 
	            		"username_rate", 
	            		$meta_detail
	            	);
		            
		            update_post_meta( 
		            	$post_id, 
		            	"rate_count", 
		            	$new_result
		            );

		            echo $new_result;
		        }
			    else {
			        echo -1;
			    }
		   	}
		    die();
		}
		else {
			echo 0;
		}
	}

	/**
	 * Prevent not logged users to rate
	 *
	 * @since    1.0.0
	 */
	public function must_login() {
	   echo 0;
	   die();
	}

	/**
	 * Function to get my post rate
	 *
	 * @since    1.0.0
	 */
	public function get_my_rate($post_id) {
	
		$rates = get_post_meta( $post_id, 'username_rate', true );

			if ( $rates and is_user_logged_in() ) {
				$current_user = wp_get_current_user()->user_login;

				foreach ($rates as $rate ) {
					if( array_key_exists($current_user, $rate) ) {
			        	return $rate[$current_user];
					}
				}
			}
			return 0;
	}

	/**
	 * Function to display the rate boxes
	 *
	 * @since    1.0.0
	 */
	public function review_post() {
		
		global $post;
		$post_id = $post->ID;		

		if ($post->post_type == 'product') {
			$my_vote = $this->get_my_rate( $post_id );
			$post_count = get_post_meta($post_id, 'rate_count', true );
			
			echo "<div class='review-area'>";
			echo "<span>Total Votes: </span>";
			echo "<span class='ntd-total-rate'>{$post_count}</span>";
			echo "<span> Your Rate: {$my_vote}</span>";
			echo "<ul class='ntd-review ntd-pa0 ntd-ma0' data-id='{$post_id}'>";
			echo "<li>Rate this product</li>";
			
			for ($i=1; $i < 6 ; $i++) { 
				echo "<li class='ntd-value " . ($i == $my_vote ? 'active' : '') ."'><a href='#'>{$i}</a></li>";
			}
			echo "</ul></div>";
			echo "<br>";
		}
	}

	/**
	 * Display nutritional data of products
	 *
	 * @since    1.0.0
	 */
	public function get_nutritional_data() {

		global $post;
		
		if ($post->post_type == 'product') {
			$diet = new NutritionalDataDiet();
			include( plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/nutritional-data-table.php' );
		}
	}

	/**
	 * Infinite products load shortcode markup maker.
	 * @since    1.0.0
	 * [products]
	 */
	function infinite_products_shortcode() {
		return '<div id="ntd-infinite-load"></div>';
	}

	/**
	 * Ajax infinite products function.
	 * @since    1.0.0
	 * [products]
	 */
	function infinite_products() { 

		$nonce = $_POST['nonce'];

	    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
	        die ( 'Cant access here!');
	     
	    if ( isset( $_POST['post_number'] ) ) {

		    $post_number = $_POST['post_number']; 
			
			$args = array(
				'post_type' 		=> 'product',
				'posts_per_page' 	=> 1,
				'offset' 			=> $post_number,
				'post_status' 		=> 'publish',
			);

			// echo "hello";
			
			$posts = new WP_Query( $args );

			if( $posts->have_posts() ):
			    while( $posts->have_posts() ): $posts->the_post();
			      echo "<article>";
			      echo "<h1>";
			      echo the_title();
			      echo "</h1>";
			      echo the_content();
			      do_action('get_nutritional_data');
			      echo "<article>";
			      echo "<hr>";
			    endwhile;
			endif;		    
		}

	    exit;
	}
}