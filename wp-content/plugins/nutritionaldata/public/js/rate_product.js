(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 */

	 $(".ntd-review .ntd-value").click(function(){
     
        // Retrieve post ID from data attribute
        var that = this;
        var product_id = $(this).parent().data('id');
        var score = $(this).children().text();
         
        // Ajax call
        $.ajax({
            type: "post",
            dataType : "json",
            url: ajax_var.url,
            data: {
            	action: 'product_rate',
            	nonce: ajax_var.nonce,
            	product_score: score,
            	product_id: product_id,
            },
            success: function(count) {
                // If vote successful
                if( count > 0 ) {
                    $(that).addClass("active");
                    $('.ntd-total-rate').text(count);
                }
                else if ( count == 0 ) {
                    $('.ntd-review').append('<p>Please Login to rate</p>');
                }
                else {
                    $('.ntd-review').append('<p>User Alredy rated</p>');
                }
            },
            error: function(error) {
                console.log(error.statusText);
            }
        });     
        return false;
    })

})( jQuery );
