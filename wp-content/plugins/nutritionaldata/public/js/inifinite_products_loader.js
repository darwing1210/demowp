(function( $ ) {
	'use strict';

	var count = 0;
	var container = "#ntd-infinite-load";
	var total_of_products = parseInt(ajax_var.total_products);
	var loading = false;
    
    $(window).scroll(function() {
        if ($(window).scrollTop() >= $(window).height() - $('.content-area').parent().height()){
        	if (count < total_of_products && !loading) {
	        	loadArticle(count, container);
        		count++;
        	}
        	else if (count == total_of_products && !loading) {
        		$(container).append('<p style="color:red;">End of posts</p>');
        		count++;
        	}
        	else {
        		return false;
        	}
        }
    }); 
 
    function loadArticle(post_number) {
    	$(container).append('<div class="ntd-loader" style="color: blue;">Loading...</div>');
        loading = true;
        $.ajax({
            type: "post",
            dataType : "html",
            url: ajax_var.url,
            data: {
            	action: 'infinite_products',
            	nonce: ajax_var.nonce,
            	post_number: post_number,
            },
            success: function(html) {
            	$(".ntd-loader").remove();
                $(container).append(html);
                loading = false;           
            },
            error: function(error) {
            	console.log(error.statusText);
            }
        });
        return false;
    }

})( jQuery );
