=== Nutritional Data ===
Contributors: darwing1210
Tags: nutrition, food, products
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

This is simple plugin that show Nutritional Data of some products

== Installation ==

1. Upload `nutritionaldata` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Activate required plugins (ACF, WP-API)

`<?php code(); // goes in backticks ?>`