<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://demo.rain.com
 * @since             1.0.0
 * @package           nutritionaldata
 *
 * @wordpress-plugin
 * Plugin Name:       Nutritional Data
 * Plugin URI:        https://github.com/DevinVinson/WordPress-Plugin-Boilerplate
 * Description:       This is a Simple Plugin that shows Nutritional Data Info.
 * Version:           1.0.0
 * Author:            Darwing Medina
 * Author URI:        http://demo.rain.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       nutritionaldata
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/NutritionalDataActivator.php
 */
function activate_plugin_name() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/NutritionalDataActivator.php';
	NutritionalDataActivator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/NutritionalDataDeactivator.php
 */
function deactivate_plugin_name() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/NutritionalDataDeactivator.php';
	NutritionalDataDeactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_plugin_name' );
register_deactivation_hook( __FILE__, 'deactivate_plugin_name' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/NutritionalData.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_plugin_name() {

	$plugin = new NutritionalData();
	$plugin->run();

}
run_plugin_name();
